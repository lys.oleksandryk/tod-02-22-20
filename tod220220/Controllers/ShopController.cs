﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using tod220220.Models;

namespace tod220220.Controllers
{
    public class ShopController : ApiController
    {
        public static List<Product> Products = new List<Product>
        {
            new Product{Amount = 100, Id = 1, Name = "apple", Price =20},
            new Product{Amount = 100, Id = 2, Name = "Banana", Price =25},
            new Product{Amount = 100, Id = 3, Name = "Kartopla", Price =5},
            new Product{Amount = 100, Id = 4, Name = "Pomidor", Price =33},
            new Product{Amount = 100, Id = 5, Name = "Mleko", Price =32}
        };

        public IEnumerable<object> ProductsInShop { get; private set; }

        // GET: api/Shop
        public List<Product> Get()
        {
            return Products;
        }

        // GET: api/Shop/5
        public Product Get(int id)
        {
            foreach (var product in Products)
            {
                if (product.Id == id)
                {
                    return product;
                }
            }
            return null;
        }
        [Route("api/Shop/buy")]
        public void Buy()
        {
            var items = new List<PurchaseItem>
            {
                new PurchaseItem{Id = 5, Amount = 3 },
                new PurchaseItem{Id = 2 , Amount = 6}
            };
            foreach (var purchaseItem in items)
            {
                foreach (var product in Products)
                {
                    if (purchaseItem.Id == product.Id && product.Amount >= purchaseItem.Amount)
                    {
                        product.Amount -= purchaseItem.Amount;
                        break;
                    }
                }
            }

        }

        // POST: api/Shop
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Shop/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Shop/5
        public void Delete(int id)
        {
        }
    }
}
