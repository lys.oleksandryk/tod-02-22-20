﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tod220220.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Amount { get; set; }
        public float Price { get; set; }
    }
}